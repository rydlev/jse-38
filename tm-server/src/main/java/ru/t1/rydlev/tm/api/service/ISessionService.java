package ru.t1.rydlev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.model.Session;

import java.util.List;

public interface ISessionService {

    @NotNull
    Session add(@NotNull final Session model) throws Exception;

    @NotNull
    Session add(@Nullable final String userId, @NotNull final Session model) throws Exception;

    void clear(@Nullable final String userId) throws Exception;

    boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception;

    int getSize(@Nullable final String userId) throws Exception;

    @Nullable
    List<Session> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    Session findOneById(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception;

    void remove(@Nullable final String userId, @Nullable final Session model) throws Exception;

    void removeById(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception;

}

