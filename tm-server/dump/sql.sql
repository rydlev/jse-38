-- Table: public.tm_project

-- DROP TABLE IF EXISTS public.tm_project;

CREATE TABLE IF NOT EXISTS public.tm_project
(
    id character varying COLLATE pg_catalog."default" NOT NULL,
    name character varying COLLATE pg_catalog."default",
    created timestamp without time zone NOT NULL,
    description character varying COLLATE pg_catalog."default",
    user_id character varying COLLATE pg_catalog."default" NOT NULL,
    status character varying COLLATE pg_catalog."default",
    CONSTRAINT tm_project_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_project
    OWNER to postgres;

-- Table: public.tm_session

-- DROP TABLE IF EXISTS public.tm_session;

CREATE TABLE IF NOT EXISTS public.tm_session
(
    id character varying COLLATE pg_catalog."default" NOT NULL,
    date timestamp without time zone NOT NULL,
    user_id character varying COLLATE pg_catalog."default" NOT NULL,
    role character varying COLLATE pg_catalog."default",
    CONSTRAINT tm_session_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_session
    OWNER to postgres;


-- Table: public.tm_task

-- DROP TABLE IF EXISTS public.tm_task;

CREATE TABLE IF NOT EXISTS public.tm_task
(
    id character varying COLLATE pg_catalog."default" NOT NULL,
    name character varying COLLATE pg_catalog."default",
    created timestamp without time zone NOT NULL,
    description character varying COLLATE pg_catalog."default",
    user_id character varying COLLATE pg_catalog."default" NOT NULL,
    status character varying COLLATE pg_catalog."default",
    project_id character varying COLLATE pg_catalog."default",
    CONSTRAINT tm_task_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_task
    OWNER to postgres;

-- Table: public.tm_user

-- DROP TABLE IF EXISTS public.tm_user;

CREATE TABLE IF NOT EXISTS public.tm_user
(
    id character varying COLLATE pg_catalog."default" NOT NULL,
    login character varying COLLATE pg_catalog."default" NOT NULL,
    password character varying COLLATE pg_catalog."default",
    email character varying COLLATE pg_catalog."default",
    locked boolean,
    first_name character varying COLLATE pg_catalog."default",
    last_name character varying COLLATE pg_catalog."default",
    middle_name character varying COLLATE pg_catalog."default",
    role character varying COLLATE pg_catalog."default",
    CONSTRAINT tm_user_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_user
    OWNER to postgres;