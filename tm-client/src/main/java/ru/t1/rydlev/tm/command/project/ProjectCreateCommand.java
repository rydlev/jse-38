package ru.t1.rydlev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.request.ProjectCreateRequest;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().createProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

}
