package ru.t1.rydlev.tm.exception.field;

public class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}
