package ru.t1.rydlev.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskChangeStatusByIdResponse extends AbstractResponse {
}
